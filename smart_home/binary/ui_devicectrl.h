/********************************************************************************
** Form generated from reading UI file 'devicectrl.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEVICECTRL_H
#define UI_DEVICECTRL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_deviceCtrl
{
public:
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *pushButton_12;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label;
    QLineEdit *lineEdit;
    QLabel *label_2;
    QPushButton *pushButton;
    QTextEdit *textEdit;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_6;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_2;
    QSlider *horizontalSlider;
    QPushButton *pushButton_3;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton_4;
    QSlider *horizontalSlider_2;
    QPushButton *pushButton_5;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton_6;
    QSlider *horizontalSlider_3;
    QPushButton *pushButton_7;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *pushButton_8;
    QVBoxLayout *verticalLayout_4;
    QPushButton *pushButton_10;
    QPushButton *pushButton_11;
    QPushButton *pushButton_9;

    void setupUi(QWidget *deviceCtrl)
    {
        if (deviceCtrl->objectName().isEmpty())
            deviceCtrl->setObjectName(QString::fromUtf8("deviceCtrl"));
        deviceCtrl->resize(500, 500);
        deviceCtrl->setAutoFillBackground(false);
        deviceCtrl->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 170, 127);"));
        verticalLayout_8 = new QVBoxLayout(deviceCtrl);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        pushButton_12 = new QPushButton(deviceCtrl);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));

        horizontalLayout_8->addWidget(pushButton_12);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_3);

        label_5 = new QLabel(deviceCtrl);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setLayoutDirection(Qt::LeftToRight);
        label_5->setStyleSheet(QString::fromUtf8("color: rgb(6, 156, 255);\n"
"font: 14pt \"Times New Roman\";"));

        horizontalLayout_8->addWidget(label_5);


        verticalLayout_8->addLayout(horizontalLayout_8);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_3 = new QLabel(deviceCtrl);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_7->addWidget(label_3);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_2);

        label_4 = new QLabel(deviceCtrl);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_7->addWidget(label_4);


        verticalLayout_8->addLayout(horizontalLayout_7);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label = new QLabel(deviceCtrl);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_5->addWidget(label);

        lineEdit = new QLineEdit(deviceCtrl);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout_5->addWidget(lineEdit);

        label_2 = new QLabel(deviceCtrl);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_5->addWidget(label_2);


        verticalLayout_7->addLayout(horizontalLayout_5);

        pushButton = new QPushButton(deviceCtrl);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setStyleSheet(QString::fromUtf8(""));

        verticalLayout_7->addWidget(pushButton);

        textEdit = new QTextEdit(deviceCtrl);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setStyleSheet(QString::fromUtf8("font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"\n"
"\n"
"\n"
""));

        verticalLayout_7->addWidget(textEdit);


        horizontalLayout_6->addLayout(verticalLayout_7);

        horizontalSpacer = new QSpacerItem(30, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        groupBox = new QGroupBox(deviceCtrl);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 0);\n"
"border-radius:15px\n"
"\n"
""));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_2 = new QPushButton(groupBox);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setEnabled(false);

        horizontalLayout->addWidget(pushButton_2);

        horizontalSlider = new QSlider(groupBox);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setEnabled(false);
        horizontalSlider->setMaximum(4);
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(horizontalSlider);

        pushButton_3 = new QPushButton(groupBox);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setEnabled(true);
        pushButton_3->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"border: 2px ;\n"
"}\n"
"\n"
""));

        horizontalLayout->addWidget(pushButton_3);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_6->addWidget(groupBox);

        groupBox_2 = new QGroupBox(deviceCtrl);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 170, 255);\n"
"border-radius:15px\n"
"\n"
""));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pushButton_4 = new QPushButton(groupBox_2);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setEnabled(false);

        horizontalLayout_2->addWidget(pushButton_4);

        horizontalSlider_2 = new QSlider(groupBox_2);
        horizontalSlider_2->setObjectName(QString::fromUtf8("horizontalSlider_2"));
        horizontalSlider_2->setEnabled(false);
        horizontalSlider_2->setMaximum(4);
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(horizontalSlider_2);

        pushButton_5 = new QPushButton(groupBox_2);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setEnabled(true);

        horizontalLayout_2->addWidget(pushButton_5);


        verticalLayout_2->addLayout(horizontalLayout_2);


        verticalLayout_6->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(deviceCtrl);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setStyleSheet(QString::fromUtf8("background-color: rgb(85, 255, 255);\n"
"border-radius:15px\n"
"\n"
""));
        verticalLayout_3 = new QVBoxLayout(groupBox_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pushButton_6 = new QPushButton(groupBox_3);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setEnabled(false);

        horizontalLayout_3->addWidget(pushButton_6);

        horizontalSlider_3 = new QSlider(groupBox_3);
        horizontalSlider_3->setObjectName(QString::fromUtf8("horizontalSlider_3"));
        horizontalSlider_3->setEnabled(false);
        horizontalSlider_3->setMaximum(4);
        horizontalSlider_3->setOrientation(Qt::Horizontal);

        horizontalLayout_3->addWidget(horizontalSlider_3);

        pushButton_7 = new QPushButton(groupBox_3);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setEnabled(true);

        horizontalLayout_3->addWidget(pushButton_7);


        verticalLayout_3->addLayout(horizontalLayout_3);


        verticalLayout_6->addWidget(groupBox_3);

        groupBox_4 = new QGroupBox(deviceCtrl);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 0);\n"
"border-radius:15px\n"
"\n"
""));
        verticalLayout_5 = new QVBoxLayout(groupBox_4);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        pushButton_8 = new QPushButton(groupBox_4);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setEnabled(false);

        horizontalLayout_4->addWidget(pushButton_8);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        pushButton_10 = new QPushButton(groupBox_4);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
        pushButton_10->setEnabled(false);

        verticalLayout_4->addWidget(pushButton_10);

        pushButton_11 = new QPushButton(groupBox_4);
        pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));
        pushButton_11->setEnabled(false);

        verticalLayout_4->addWidget(pushButton_11);


        horizontalLayout_4->addLayout(verticalLayout_4);

        pushButton_9 = new QPushButton(groupBox_4);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setEnabled(true);

        horizontalLayout_4->addWidget(pushButton_9);


        verticalLayout_5->addLayout(horizontalLayout_4);


        verticalLayout_6->addWidget(groupBox_4);


        horizontalLayout_6->addLayout(verticalLayout_6);


        verticalLayout_8->addLayout(horizontalLayout_6);


        retranslateUi(deviceCtrl);

        QMetaObject::connectSlotsByName(deviceCtrl);
    } // setupUi

    void retranslateUi(QWidget *deviceCtrl)
    {
        deviceCtrl->setWindowTitle(QCoreApplication::translate("deviceCtrl", "Form", nullptr));
        pushButton_12->setText(QCoreApplication::translate("deviceCtrl", "back", nullptr));
        label_5->setText(QCoreApplication::translate("deviceCtrl", "Smart Home", nullptr));
        label_3->setText(QCoreApplication::translate("deviceCtrl", "\346\230\276\347\244\272\345\231\250", nullptr));
        label_4->setText(QCoreApplication::translate("deviceCtrl", "\346\216\247\345\210\266\345\231\250", nullptr));
        label->setText(QCoreApplication::translate("deviceCtrl", "\346\270\251\345\272\246\357\274\232", nullptr));
        label_2->setText(QCoreApplication::translate("deviceCtrl", "\345\272\246", nullptr));
        pushButton->setText(QCoreApplication::translate("deviceCtrl", "clear", nullptr));
        groupBox->setTitle(QString());
        pushButton_2->setText(QString());
        pushButton_3->setText(QString());
        groupBox_2->setTitle(QString());
        pushButton_4->setText(QString());
        pushButton_5->setText(QString());
        groupBox_3->setTitle(QString());
        pushButton_6->setText(QString());
        pushButton_7->setText(QString());
        groupBox_4->setTitle(QString());
        pushButton_8->setText(QString());
        pushButton_10->setText(QString());
        pushButton_11->setText(QString());
        pushButton_9->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class deviceCtrl: public Ui_deviceCtrl {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEVICECTRL_H
