#include "sendrequestmessage.h"

SendRequestMessage::SendRequestMessage()
{

}

QNetworkReply* SendRequestMessage::send_request_message( const QString ct, QString http_url, QNetworkAccessManager *manager,QNetworkReply *reply)
{
    //QString http_url = "http://wthrcdn.etouch.cn/weather_mini?city=" + ct;
    http_url = http_url+ct;
    qDebug()<<http_url;
    //1.创建一个http请求类对象，填充到对应的数据中


    QNetworkRequest http_request(http_url);

    //2.向服务器发送http请求
    reply = manager->get(http_request);
    return reply;
    //qDebug()<<"reply"<<reply;
//    //3.关联信号与槽函数，当云服务器向我们回复数据结束的时候，系统会触发一个叫做finish信号
//    QObject::connect(reply,SIGNAL(finished()),this,SLOT(deal_reply_msg()));
}
