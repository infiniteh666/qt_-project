#ifndef MUSIC_H
#define MUSIC_H

#include <QWidget>
#include <QMediaPlayer>
#include <QListWidgetItem>

namespace Ui {
class music;
}

class music : public QWidget
{
    Q_OBJECT

public:
    explicit music(QWidget *parent = nullptr);
    ~music();

    void loadLocalMusic();

signals:
    void menuhide();
    void DoubleClicked(QListWidgetItem *item);
    void menushow();
private slots:
    void sethide();
    void slot_DoubleClicked(QListWidgetItem *item);

    void on_pushButton_2_clicked();


    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_verticalSlider_valueChanged(int value);

private:
    Ui::music *ui;
    QString musicPath;
    bool flag = false;
    QMediaPlayer *music_1;
    int volume = 30;
};

#endif // MUSIC_H
