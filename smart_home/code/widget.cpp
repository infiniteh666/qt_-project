#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    if(ui->lineEdit->text() == "abc" && ui->lineEdit_2->text() == "123")
    {
        emit loading();
        this->setHidden(true);
    }
}


