#ifndef MENU_H
#define MENU_H

#include <QWidget>

namespace Ui {
class Menu;
}

class Menu : public QWidget
{
    Q_OBJECT

public:
    explicit Menu(QWidget *parent = nullptr);
    ~Menu();

signals:
    void loadmusic();
    void weathershow();
    void deviceshow();
private slots:
    void sethide();
    void menuhide_slot();
    void menushow_slot();
    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::Menu *ui;
};

#endif // MENU_H
