#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>
#include "data_utf8_encode.h"
#include "sendrequestmessage.h"
#include "dealreplymsg.h"

class mythread : public QThread
{
    Q_OBJECT
public:
    mythread();
    mythread(QNetworkAccessManager *man,QNetworkReply *rep);
    virtual void run();

signals:
    void send_QJsonObject(QJsonObject);

private slots:
    void getcity(QString ct){city = ct;}
    void deal_data();

private:
    QNetworkAccessManager *manager; //定义网络操作对象类
    QNetworkReply *reply;
    QThread *thread;
    QString city = "";

};

#endif // MYTHREAD_H
