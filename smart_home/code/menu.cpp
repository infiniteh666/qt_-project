#include "menu.h"
#include "ui_menu.h"

Menu::Menu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Menu)
{
    ui->setupUi(this);
    this->setHidden(true);

    this->setWindowTitle("Menu");
}

Menu::~Menu()
{
    delete ui;
}

void Menu::sethide()
{
    this->setHidden(false);
}

void Menu::menuhide_slot()
{
    this->setHidden(true);
}

void Menu::menushow_slot()
{
    this->setHidden(false);
}

void Menu::on_pushButton_3_clicked()
{
    this->setHidden(true);
    emit loadmusic();
}



void Menu::on_pushButton_2_clicked()
{
    this->setHidden(true);
    emit weathershow();
}

void Menu::on_pushButton_clicked()
{
    this->setHidden(true);
    emit deviceshow();
}
