#include "dealreplymsg.h"

dealReplyMsg::dealReplyMsg(QObject *parent) : QObject(parent)
{

}

QJsonObject dealReplyMsg::deal_reply_msg(QNetworkReply *reply)
{
    //1.读取所有的数据----Json类型的数据
    QByteArray data = reply->readAll();

    //2.将QByteArray转为QJsonDocument对象
    QJsonDocument Joc = QJsonDocument::fromJson(data);

    //3.将QJsonDocument对象转换为QJsonObject对象
    QJsonObject  json_obj = Joc.object();

  //  qDebug() << json_obj;

    //4.获得服务器回复的data数据中的值
    QJsonObject dataobj = json_obj["data"].toObject();

    QJsonArray forecast = dataobj["forecast"].toArray();

    //获得今天的信息
    QJsonObject todayobj = forecast[0].toObject();
    return todayobj;
}
