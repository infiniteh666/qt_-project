#include "weather.h"
#include "ui_weather.h"

#include <QLineEdit>

Weather::Weather(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Weather),
    manager(new QNetworkAccessManager)
{
    ui->setupUi(this);

    thread = new mythread(manager,reply);

    connect(this,SIGNAL(cityinfo(QString)),thread,SLOT(getcity(QString)));
    connect(thread,SIGNAL(send_QJsonObject(QJsonObject)),this,SLOT(slot_QJsonObject(QJsonObject)));
}

Weather::~Weather()
{
    delete ui;
    delete manager;
    delete thread;
}

void Weather::on_pushButton_clicked()
{

    ct = ui->lineEdit->text();
    emit cityinfo(ct);
//while(thread->isRunning())
//{
//    QThread::sleep(100);
//}

    thread->start();        //这里线程只启动了一次，等二次启动就没用了

}

void Weather::slot_QJsonObject(QJsonObject aaa)
{
    if (thread->isFinished() == false)
      {
        thread->exit();
      }

    wind_direction = aaa["fengxiang"].toString();
    wind_power = aaa["fengli"].toString();
    min_temprature = aaa["low"].toString();
    max_temprature = aaa["high"].toString();
    type = aaa["type"].toString();
    ui->label_5->setText(min_temprature+"--"+max_temprature);
    ui->label_6->setText(wind_direction);
    ui->label_7->setText(wind_power);
    ui->label_10->setText(type);
    qDebug()<<"风向"<<wind_direction;
    qDebug()<<"Json:"<<aaa;
    qDebug()<<wind_power;

}


void Weather::on_pushButton_2_clicked()
{
    this->setHidden(true);
    emit menushow();
}
