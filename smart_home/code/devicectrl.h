#ifndef DEVICECTRL_H
#define DEVICECTRL_H

#include <QWidget>

namespace Ui {
class deviceCtrl;
}

class deviceCtrl : public QWidget
{
    Q_OBJECT

public:
    explicit deviceCtrl(QWidget *parent = nullptr);
    ~deviceCtrl();
signals:
    void menushow();

private slots:
    void deviceshow_slot(){this->setHidden(false);}
    void on_pushButton_10_clicked();

    void on_pushButton_11_clicked();


    void on_horizontalSlider_valueChanged(int value);

    void on_horizontalSlider_2_valueChanged(int value);

    void on_horizontalSlider_3_valueChanged(int value);


    void on_pushButton_9_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_clicked();

    void on_pushButton_12_clicked();

private:
    Ui::deviceCtrl *ui;
    int temprature = 20;
    int light = 0;
    int slotlight = 0;
    int volume = 0;

     bool airbutton = false;
     bool lightbutton = false;
     bool slotlightbutton = false;
     bool playerbutton = false;
};

#endif // DEVICECTRL_H
