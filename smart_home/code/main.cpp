#include "widget.h"
#include "menu.h"
#include "music.h"
#include "weather.h"
#include "devicectrl.h"

#include <QApplication>
#include <QObject>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Widget w;
    Menu m;
    music mc;
    Weather wea;
    deviceCtrl d;

    QObject::connect(&w,SIGNAL(loading()),&m,SLOT(sethide()));
    QObject::connect(&m,SIGNAL(loadmusic()),&mc,SLOT(sethide()));

    QObject::connect(&mc,SIGNAL(menuhide()),&m,SLOT(menuhide_slot()));
    QObject::connect(&mc,SIGNAL(menushow()),&m,SLOT(menushow_slot()));

    QObject::connect(&m,SIGNAL(weathershow()),&wea,SLOT(weathershow_slot()));
    QObject::connect(&wea,SIGNAL(menushow()),&m,SLOT(menushow_slot()));

    QObject::connect(&m,SIGNAL(deviceshow()),&d,SLOT(deviceshow_slot()));
    QObject::connect(&d,SIGNAL(menushow()),&m,SLOT(menushow_slot()));
    w.show();
    return a.exec();
}
