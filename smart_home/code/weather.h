#ifndef WEATHER_H
#define WEATHER_H

#include <QWidget>
#include "data_utf8_encode.h"
#include "dealreplymsg.h"
#include "sendrequestmessage.h"
#include <mythread.h>

namespace Ui {
class Weather;
}

class Weather : public QWidget
{
    Q_OBJECT

public:
    explicit Weather(QWidget *parent = nullptr);
    ~Weather();

signals:
    void cityinfo(QString str);
    void menushow();

private slots:
    void on_pushButton_clicked();

    void weathershow_slot(){this->setHidden(false);}

    void slot_QJsonObject(QJsonObject aaa);

    void on_pushButton_2_clicked();

private:
    Ui::Weather *ui;
    QNetworkAccessManager *manager; //定义网络操作对象类
    QNetworkReply *reply;
    QThread *thread;

    QString ct = "";
    QString max_temprature="";
    QString min_temprature="";
    QString wind_direction = "";
    QString wind_power = "";
    QString type = "";

};

#endif // WEATHER_H
