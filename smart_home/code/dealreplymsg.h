#ifndef DEALREPLYMSG_H
#define DEALREPLYMSG_H

#include <QObject>
#include <QNetworkReply>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>

class dealReplyMsg : public QObject
{
    Q_OBJECT
public:
    explicit dealReplyMsg(QObject *parent = nullptr);

    QJsonObject deal_reply_msg(QNetworkReply *reply);

signals:

};

#endif // DEALREPLYMSG_H
