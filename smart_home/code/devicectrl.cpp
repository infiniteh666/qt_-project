#include "devicectrl.h"
#include "ui_devicectrl.h"
#include <QDebug>

deviceCtrl::deviceCtrl(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::deviceCtrl)
{
    ui->setupUi(this);
    ui->lineEdit->setText(QString("%1").arg(temprature));
    this->setStyleSheet("background-image:url(:/image/icon/1.webp)");

    ui->pushButton_2->setIcon(QIcon(":/image/icon/bumb.png"));
    ui->pushButton_2->setIconSize(QSize(40,40));
    ui->pushButton_2->setText("灯光");
    ui->pushButton_2->setFlat(true);//设置按钮无边框

    ui->pushButton_3->setIcon(QIcon(":/image/icon/button.png"));
    ui->pushButton_3->setIconSize(QSize(40,40));
    ui->pushButton_3->setFlat(true);//设置按钮无边框

    ui->pushButton_4->setIcon(QIcon(":/image/icon/slotbumb.png"));
    ui->pushButton_4->setIconSize(QSize(40,40));
    ui->pushButton_4->setText("射灯");
    ui->pushButton_4->setFlat(true);//设置按钮无边框

    ui->pushButton_5->setIcon(QIcon(":/image/icon/button.png"));
    ui->pushButton_5->setIconSize(QSize(40,40));
    ui->pushButton_5->setFlat(true);//设置按钮无边框

    ui->pushButton_6->setIcon(QIcon(":/image/icon/player.png"));
    ui->pushButton_6->setIconSize(QSize(40,40));
    ui->pushButton_6->setText("音响");
    ui->pushButton_6->setFlat(true);//设置按钮无边框

    ui->pushButton_7->setIcon(QIcon(":/image/icon/button.png"));
    ui->pushButton_7->setIconSize(QSize(40,40));
    ui->pushButton_7->setFlat(true);//设置按钮无边框

    ui->pushButton_8->setIcon(QIcon(":/image/icon/aircondition.png"));
    ui->pushButton_8->setIconSize(QSize(40,40));
    ui->pushButton_8->setText("空调");
    ui->pushButton_8->setFlat(true);//设置按钮无边框

    ui->pushButton_9->setIcon(QIcon(":/image/icon/button.png"));
    ui->pushButton_9->setIconSize(QSize(40,40));
    ui->pushButton_9->setFlat(true);//设置按钮无边框

    ui->pushButton_10->setIcon(QIcon(":/image/icon/up.png"));
    ui->pushButton_10->setIconSize(QSize(41,41));
    ui->pushButton_10->setFlat(true);//设置按钮无边框

    ui->pushButton_11->setIcon(QIcon(":/image/icon/down.png"));
    ui->pushButton_11->setIconSize(QSize(35,35));
    ui->pushButton_11->setFlat(true);//设置按钮无边框


}

deviceCtrl::~deviceCtrl()
{
    delete ui;
}

void deviceCtrl::on_pushButton_10_clicked()
{
    ui->lineEdit->setText(QString("%1").arg(++temprature));
}

void deviceCtrl::on_pushButton_11_clicked()
{
    ui->lineEdit->setText(QString("%1").arg(--temprature));
}


//void deviceCtrl::on_pushButton_9_pressed()
//{
//    ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("空调开关已打开！"));
//    ui->pushButton_10->setEnabled(true);
//    ui->pushButton_11->setEnabled(true);
//}

//void deviceCtrl::on_pushButton_9_released()
//{
//    ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("空调开关已关闭！"));
//    ui->pushButton_10->setEnabled(false);
//    ui->pushButton_11->setEnabled(false);
//}

void deviceCtrl::on_horizontalSlider_valueChanged(int value)
{
    ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("设置灯光亮度为：%1").arg(value));
}

void deviceCtrl::on_horizontalSlider_2_valueChanged(int value)
{
    ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("设置射灯亮度为：%1").arg(value));
}


void deviceCtrl::on_horizontalSlider_3_valueChanged(int value)
{
    ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+ QString("设置音响亮度为：%1").arg(value));
}



void deviceCtrl::on_pushButton_9_clicked()
{
    if(!airbutton)
    {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("空调开关已打开！"));
            ui->pushButton_10->setEnabled(true);
            ui->pushButton_11->setEnabled(true);
            airbutton = true;
    }
    else
    {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("空调开关已关闭！"));
            ui->pushButton_10->setEnabled(false);
            ui->pushButton_11->setEnabled(false);
            airbutton = false;
    }
}

void deviceCtrl::on_pushButton_3_clicked()
{
    if(!lightbutton)
    {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("灯泡开关已打开！"));
            ui->horizontalSlider->setEnabled(true);
            lightbutton = true;
    }
    else
    {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("灯泡开关已关闭！"));
            ui->horizontalSlider->setEnabled(false);
            lightbutton = false;
    }
}

void deviceCtrl::on_pushButton_5_clicked()
{
    if(!slotlightbutton)
    {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("射灯开关已打开！"));
            ui->horizontalSlider_2->setEnabled(true);
            slotlightbutton = true;
    }
    else
    {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("射灯开关已关闭！"));
            ui->horizontalSlider_2->setEnabled(false);
            slotlightbutton = false;
    }
}

void deviceCtrl::on_pushButton_7_clicked()
{
    if(!playerbutton)
    {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("射灯开关已打开！"));
            ui->horizontalSlider_3->setEnabled(true);
            playerbutton = true;
    }
    else
    {
            ui->textEdit->setText(ui->textEdit->toPlainText()+"\n"+QString("射灯开关已关闭！"));
            ui->horizontalSlider_3->setEnabled(false);
            playerbutton = false;
    }
}

void deviceCtrl::on_pushButton_clicked()
{
    ui->textEdit->clear();
}

void deviceCtrl::on_pushButton_12_clicked()
{
    this->setHidden(true);
    emit menushow();
}
