#ifndef SENDREQUESTMESSAGE_H
#define SENDREQUESTMESSAGE_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>


class SendRequestMessage:public QObject
{
    Q_OBJECT
public:
    SendRequestMessage();

    QNetworkReply* send_request_message(const QString ct,const QString httpurl,QNetworkAccessManager *manager,QNetworkReply *reply);
};

#endif // SENDREQUESTMESSAGE_H
