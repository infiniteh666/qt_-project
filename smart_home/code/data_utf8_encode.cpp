#include "data_utf8_encode.h"

data_utf8_encode::data_utf8_encode()
{

}

void data_utf8_encode::data_Utf8_Encode(QString &data)
{
    //1.创建一个utf-8编码格式的指针
    QTextCodec *codec = QTextCodec::codecForName("utf-8");


    //2.把中文字符填充到该类中
    QByteArray  by = codec->fromUnicode(data);

    //3.对汉字进行url编码
    QByteArray encoded = by.toPercentEncoding();

    //4.转换为QString相关的字符串信息
    data.clear();  //删除原来的数据
    data = encoded.data();
}
