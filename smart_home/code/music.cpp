#include "music.h"
#include "ui_music.h"
#include <QDirIterator>
#include <QDebug>
#include <QListWidget>

music::music(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::music),
    music_1(new QMediaPlayer)
{
    ui->setupUi(this);

    qRegisterMetaType<QListWidgetItem>("QListWidgetItem");
    connect(this,SIGNAL(DoubleClicked(QListWidgetItem*)),this,SLOT(slot_DoubleClicked(QListWidgetItem*)));

    musicPath = "./music";

    QIcon myicon1(":/image/icon/1.png"); //新建QIcon对象
    QIcon myicon2(":/image/icon/5.png"); //新建QIcon对象
    QIcon myicon3(":/image/icon/2.png"); //新建QIcon对象

    ui->pushButton->setIcon(myicon1); //给按钮添加图标
    ui->pushButton->setFlat(true);
    ui->pushButton->setIconSize(QSize(50,50));//重置图标大小

    //ui->pushButton_2->setIcon(QIcon(tr(":/image/icon/4.png"))); //给按钮添加图标
    ui->pushButton_2->setIcon(myicon2); //给按钮添加图标
    ui->pushButton_2->setFlat(true);
    ui->pushButton_2->setIconSize(QSize(40,40));//重置图标大小

    ui->pushButton_3->setIcon(myicon3); //给按钮添加图标
    ui->pushButton_3->setFlat(true);
    ui->pushButton_3->setIconSize(QSize(50,50));//重置图标大小'

    ui->pushButton_6->setIcon(QIcon(":/image/icon/pause.png"));
    ui->pushButton_6->setFlat(true);
    ui->pushButton_6->setIconSize(QSize(40,40));//重置图标大小'

    loadLocalMusic();   //导入本地音乐列表
    music_1->setMedia(QUrl(musicPath+"/"+ui->listWidget->item(0)->text()));
    music_1->setVolume(volume);
}

music::~music()
{
    delete ui;
}

void music::sethide()
{
    this->setHidden(false);
    emit menuhide();
}

void music::slot_DoubleClicked(QListWidgetItem *item)
{
    item->setSelected(true);
    music_1->setMedia(QUrl(musicPath+"/"+item->text()));
    music_1->play();
    ui->pushButton_2->setIcon(QIcon(tr(":/image/icon/4.png"))); //给按钮添加图标
    ui->pushButton_2->setIconSize(QSize(50,50));//重置图标大小
}

void music::loadLocalMusic()
{
    QDirIterator dir_iterator("./music");
    //QDirIterator dir_iterator("music");

    while(dir_iterator.hasNext())
    {
        dir_iterator.next();
        QFileInfo file_info = dir_iterator.fileInfo();
        QString file_name = file_info.fileName();
        qDebug() << "filename:" << file_name;
        //不要"."和".."两个文件夹
        if(file_name != "." && file_name != "..")
        {
           ui->listWidget->addItem(file_name);
        }
    }

}


void music::on_pushButton_2_clicked()
{
    if(flag)
    {
        music_1->pause();
        ui->pushButton_2->setIcon(QIcon(tr(":/image/icon/5.png"))); //给按钮添加图标
        ui->pushButton_2->setIconSize(QSize(40,40));//重置图标大小
        flag = false;
    }
    else
    {
        music_1->play();
        ui->pushButton_2->setIcon(QIcon(tr(":/image/icon/4.png"))); //给按钮添加图标
        ui->pushButton_2->setIconSize(QSize(50,50));//重置图标大小
        flag = true;
    }
}


void music::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    music_1->setMedia(QUrl(musicPath+"/"+item->text()));
    music_1->play();
    ui->pushButton_2->setIcon(QIcon(tr(":/image/icon/4.png"))); //给按钮添加图标
    ui->pushButton_2->setIconSize(QSize(50,50));//重置图标大小

}

void music::on_pushButton_clicked()
{
   // ui->listWidget->setCurrentRow();//原来还可以设置当前行啊；soga
    ui->listWidget->currentItem()->setSelected(false);
    if(ui->listWidget->currentRow() == 0)
    {
        emit DoubleClicked(ui->listWidget->item(ui->listWidget->count()-1));
        ui->listWidget->setCurrentRow(ui->listWidget->count()-1);
    }

    else
    {
        emit DoubleClicked(ui->listWidget->item(ui->listWidget->currentRow()-1));
        ui->listWidget->setCurrentRow(ui->listWidget->currentRow()-1);
    }

}

void music::on_pushButton_3_clicked()
{
    ui->listWidget->currentItem()->setSelected(false);
//    qDebug()<<"currentrow: "<<ui->listWidget->currentRow();
//    qDebug()<<"ui->listWidget->count():"<<ui->listWidget->count();
    if(ui->listWidget->currentRow() == ui->listWidget->count()-1)
    {
        emit DoubleClicked(ui->listWidget->item(0));
        ui->listWidget->setCurrentRow(0);
    }

    else
    {
        emit DoubleClicked(ui->listWidget->item(ui->listWidget->currentRow()+1));
        ui->listWidget->setCurrentRow(ui->listWidget->currentRow()+1);
    }
}

void music::on_pushButton_4_clicked()
{
    music_1->setVolume(++volume);
}

void music::on_pushButton_5_clicked()
{
    music_1->setVolume(--volume);
}

void music::on_pushButton_6_clicked()
{
    music_1->stop();
    ui->pushButton_2->setIcon(QIcon(tr(":/image/icon/5.png"))); //给按钮添加图标
    ui->pushButton_2->setIconSize(QSize(40,40));//重置图标大小
}

void music::on_pushButton_7_clicked()
{
    this->setHidden(true);
    music_1->stop();
    emit menushow();
}

void music::on_verticalSlider_valueChanged(int value)
{
    volume = value;
    qDebug()<<volume;
    music_1->setVolume(volume);
}
