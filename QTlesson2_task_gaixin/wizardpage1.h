#ifndef WIZARDPAGE1_H
#define WIZARDPAGE1_H
#include <QWizardPage>

class WizardPage1 : public QWizardPage
{
public:
    explicit WizardPage1(QWidget *parent = nullptr);
    virtual bool isComplete() const;
    void SetNextEnable(bool bflag);

private:
    bool bt_nextDisable;
};

#endif // WIZARDPAGE1_H
