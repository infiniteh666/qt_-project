#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QObject>
#include <QThread>
#include <QDebug>
class mythread : public QThread
{
    Q_OBJECT
public:

    explicit mythread(QObject *parent = nullptr);



signals:
    void Sig_ProgressValue(int);
    void absolutepath(QString);
    void Sig_ProgressMaxValue(int);

private slots:
    void getPath(QString str){path = str;qDebug()<<path;}


protected:
    virtual void run();

private:
    QString path = "D:";

};

#endif // MYTHREAD_H
