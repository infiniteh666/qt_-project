#ifndef THREADSTART_H
#define THREADSTART_H

#include <QObject>

class threadstart : public QObject
{
    Q_OBJECT
public:
    explicit threadstart(QObject *parent = nullptr);

signals:

public slots:
    bool threadbegin(){return true;}

private:
    bool m_flag = false;
};

#endif // THREADSTART_H
