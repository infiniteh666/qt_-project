#include "wizard.h"
#include "ui_wizard.h"
#include "mythread.h"
#include <QFileDialog>
#include <QFileInfo>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QDirIterator>

Wizard::Wizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::Wizard)
{
    ui->setupUi(this);

    connect(ui->checkBox, SIGNAL(toggled(bool)), this, SLOT(slot_checkbox()));
    connect(ui->checkBox, SIGNAL(toggled(bool)), ui->wizardPage1, SIGNAL(completeChanged()));   //勾选框和wizardpage的completechanged关联

    thread = new mythread();

    QObject::connect(thread, SIGNAL(Sig_ProgressValue(int)),this, SLOT(slot_ProgressValue(int)));
    QObject::connect(this, SIGNAL(pathsend(QString)),thread, SLOT(getPath(QString )));
    QObject::connect(thread, SIGNAL(absolutepath(QString)),this, SLOT(listitemAdd(QString)));
    QObject::connect(thread, SIGNAL(Sig_ProgressMaxValue(int)),this, SLOT(slot_ProgressMaxValue(int)));

}

Wizard::~Wizard()
{
    delete ui;
}


void Wizard::initializePage(int id)
{
    if(page(id) == ui->wizardPage)
    {

        thread->start();
    }
}

void Wizard::slot_checkbox()
{

    ui->wizardPage1->SetNextEnable(ui->checkBox->isChecked());
}


void Wizard::on_checkBox_clicked(bool checked)
{
    ui->wizardPage1->SetNextEnable(ui->checkBox->isChecked());
}

void Wizard::slot_ProgressValue(int ivalue)
{
    ui->progressBar->setValue(ivalue);
}

void Wizard::slot_ProgressMaxValue(int value)
{
     ui->progressBar->setMaximum(value);
}

void Wizard::on_pushButton_clicked()
{
    QString filename = QFileDialog::getExistingDirectory(this,tr("文件对话框"),tr("C:"));
    ui->lineEdit->setText(filename);
    emit pathsend(filename);
}

void Wizard::listitemAdd(QString str)
{
    ui->listWidget->addItem(str);
}



void Wizard::on_comboBox_activated(const QString &arg1)
{
    if(arg1 == "序列号")
    {
        ui->page_2->show();
        ui->page->hide();
    }
    else
    {
        ui->page->show();
        ui->page_2->hide();
    }
}
