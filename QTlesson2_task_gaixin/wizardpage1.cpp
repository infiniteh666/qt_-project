#include "wizardpage1.h"


WizardPage1::WizardPage1(QWidget *parent)
    :QWizardPage(parent)
{
    bt_nextDisable = true;
}

bool WizardPage1::isComplete() const
{
    return !bt_nextDisable;
}

void WizardPage1::SetNextEnable(bool bflag)
{
    bt_nextDisable = !bflag;
}

