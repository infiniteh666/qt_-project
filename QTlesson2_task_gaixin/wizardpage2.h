#ifndef WIZARDPAGE2_H
#define WIZARDPAGE2_H
#include<QThread>
#include "wizard.h"

class wizardPage2 : public QThread
{
public:
    wizardPage2();

signals:
    void Sig_ProgressValue(int);

protected:
    virtual void run(QString str);

};

#endif // WIZARDPAGE2_H
