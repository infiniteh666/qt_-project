#include "mythread.h"
#include "wizard.h"
#include <QFileDialog>
#include <QFileInfo>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QDirIterator>

mythread::mythread(QObject *parent)
    :QThread(parent)
{


}



void mythread::run()
{
    //QString str = "E:\mytestcode";
    QString str = path;
        size_t flag = 0;
        size_t flagnow = 0;
        QStringList filters;    //不设置默认所有文件


        QDirIterator dir_iterator(str,
            filters,
            QDir::Files | QDir::NoSymLinks,
            QDirIterator::Subdirectories);

        while(dir_iterator.hasNext())
        {
            dir_iterator.next();
            flag++;
            qDebug()<<flag;
        }
        emit Sig_ProgressMaxValue(flag);
        QDirIterator dir_iterator1(str,
            filters,
            QDir::Files | QDir::NoSymLinks,
            QDirIterator::Subdirectories);
        while(dir_iterator1.hasNext())
        {
            dir_iterator1.next();
            QFileInfo file_info = dir_iterator1.fileInfo();
            QString absolute_file_path = file_info.absoluteFilePath();
            //qDebug()<<absolute_file_path;
            ++flagnow;
            emit Sig_ProgressValue(flagnow);
            emit absolutepath(absolute_file_path);
            msleep(100);
        }
}
