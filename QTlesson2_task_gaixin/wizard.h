#ifndef WIZARD_H
#define WIZARD_H

#include <QWizard>
#include "mythread.h"

namespace Ui {
class Wizard;
}

class Wizard : public QWizard
{
    Q_OBJECT

public:
    explicit Wizard(QWidget *parent = nullptr);
    ~Wizard();
    void initializePage(int id);
    //void threadstart(mythread* aaa){aaa->start();}

signals:
    void pathsend(QString);

public slots:
    void slot_checkbox();

private slots:
    void on_checkBox_clicked(bool checked);
    void slot_ProgressValue(int value);
    void slot_ProgressMaxValue(int value);
    void on_pushButton_clicked();
    void listitemAdd(QString str);


    void on_comboBox_activated(const QString &arg1);

private:
    Ui::Wizard *ui;
    mythread*thread;
};

#endif // WIZARD_H
